//
//  BaseXCTestCase.swift
//  Tests iOS
//
//  Created by Milos Dimic on 13.05.22.
//

import XCTest

class BaseXCTestCase: XCTestCase {
    
    lazy var app: XCUIApplication = {
        
        let app = XCUIApplication()

        // Launch arguments for UNIT Tests are passed in scheme settings
        // Launch arguments for UI Tests are passed in the line below
        app.launchArguments.append(GlobalConstants.LaunchArguments.uiTest.rawValue)
        
        return app
    }()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
}
