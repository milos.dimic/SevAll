//
//  SevAllTests+Injection.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

import Resolver

extension Resolver {

    static var tests: Resolver!

    static func resetRegistrations() {

        Resolver.tests = Resolver(child: .main)
        Resolver.root = .tests
    }
}
