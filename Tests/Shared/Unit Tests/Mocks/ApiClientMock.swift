//
//  ApiClientMock.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

@testable import SevAll

final class ApiClientMock: APIClientRepresentable {
    
    func bar() -> String {
        "unit test"
    }
}
