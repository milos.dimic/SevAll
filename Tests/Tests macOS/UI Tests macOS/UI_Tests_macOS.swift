//
//  UI_Tests_macOS.swift
//  UI Tests macOS
//
//  Created by Milos Dimic on 13.05.22.
//

import XCTest

class UI_Tests_macOS: BaseXCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        self.continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        self.app.launch()
        self.app.buttons.element.firstMatch.tap()
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
}
