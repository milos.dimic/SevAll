//
//  HomeViewModelTests.swift
//  Unit Tests iOS
//
//  Created by Milos Dimic on 15.05.22.
//

import XCTest
import Resolver
@testable import SevAll

class HomeViewModelTests: XCTestCase {
    
    private var sut: HomeViewModel!
    private var apiClient: ApiClientMock!

    override func setUpWithError() throws {
        
        Resolver.resetRegistrations()
        
        self.apiClient = .init()
        
        Resolver.tests.register {
            self.apiClient
        }
        .implements(APIClientRepresentable.self)
        
        self.sut = ViewModelFactory.makeHomeViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Foo() throws {
        XCTAssertEqual("unit test", self.sut.foo())
    }
}
