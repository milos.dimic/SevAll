//
//  TestEnvironment.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

enum TestEnvironment {
    
    case unit
    case ui
    case none
    
    init(commandLine: CommandLine.Type) {
        
        let arguments = commandLine.arguments
        
        if arguments.contains((GlobalConstants.LaunchArguments.unitTest.rawValue)) {
            self = .unit
        } else if arguments.contains((GlobalConstants.LaunchArguments.uiTest.rawValue)) {
            self = .ui
        } else {
            self = .none
        }
    }
}
