//
//  App+Injection.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

import Resolver

extension Resolver: ResolverRegistering {
    
    public static func registerAllServices() {
        
        let isTestingUI = EnvironmentHelper.shared.testEnvironment == .ui
        self.registerClients(isTestingUI: isTestingUI)
        self.registerNetworkLayer(isTestingUI: isTestingUI)
        self.registerPersistenceLayer(isTestingUI: isTestingUI)
    }
}

