//
//  GlobalConstants.swift
//  SevAll
//
//  Created by Milos Dimic on 13.05.22.
//


/// Available for all Target Memberships
enum GlobalConstants {
    
    enum LaunchArguments: String {
        
        case unitTest
        case uiTest
    }
}
