//
//  AppLauncher.swift
//  SevAll
//
//  Created by Milos Dimic on 13.05.22.
//

import Foundation

@main
enum AppLauncher {
    
    static func main() {
        
        let testEnvironment = TestEnvironment(commandLine: CommandLine.self)
        EnvironmentHelper.shared.testEnvironment = testEnvironment
        
        switch testEnvironment {
        case .unit:
            return TestingApp.main()
        case .ui, .none:
            return SevAllApp.main()
        }
    }
}
