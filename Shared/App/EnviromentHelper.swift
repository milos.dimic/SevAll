//
//  EnvironmentHelper.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

struct EnvironmentHelper {
    
    static var shared = EnvironmentHelper()
    
    var testEnvironment: TestEnvironment = .none
    
    private init() {}
}
