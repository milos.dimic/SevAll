//
//  UserMapper.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

import Foundation
import SevAllNetworkLayer

struct UserMapper {
    
    typealias Model = UserModel
    typealias DBModel = UserDBModel
    typealias APIModel = UserAPIModel
}

// MARK: - PresentationMapperProtocol

extension UserMapper: PresentationMapperProtocol {
    
    static func toPresentationModel(from apiModel: UserAPIModel) -> UserModel? {
        .init()
    }
}

// MARK: - APIMapperProtocol

extension UserMapper: APIMapperProtocol {
    
    static func toAPIModel(from model: UserModel) -> UserAPIModel {
        .init()
    }
}

// MARK: - DatabaseMapperProtocol

extension UserMapper: DatabaseMapperProtocol {
    
    static func toDatabaseModel(from model: UserModel) -> UserDBModel {
        
        let dbModel = UserDBModel()
        
        return dbModel
    }
    
    static func toPresentationModel(from dbModel: UserDBModel) -> UserModel? {
        .init()
    }
}

// MARK: - PresentationConvertible

extension UserAPIModel: PresentationConvertible {
    
    var asPresentationModel: UserModel? {
        UserMapper.toPresentationModel(from: self)
    }
}

extension UserDBModel: PresentationConvertible {
    
    var asPresentationModel: UserModel? {
        UserMapper.toPresentationModel(from: self)
    }
}

// MARK: - DatabaseConvertible

extension UserModel: DatabaseConvertible {
    
    var asDatabaseModel: UserDBModel {
        UserMapper.toDatabaseModel(from: self)
    }
}

// MARK: - APIConvertible

extension UserModel: APIConvertible {
    
    var asAPIModel: UserAPIModel {
        UserMapper.toAPIModel(from: self)
    }
}
