//
//  MapperProtocols+Convertible.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

// MARK: - Protocols

protocol PresentationMapperProtocol {

    associatedtype Model
    associatedtype APIModel

    static func toPresentationModel(from apiModel: APIModel) -> Model?
}

protocol APIMapperProtocol {

    associatedtype Model
    associatedtype APIModel

    static func toAPIModel(from model: Model) -> APIModel
}

protocol DatabaseMapperProtocol {

    associatedtype Model
    associatedtype DBModel

    static func toDatabaseModel(from model: Model) -> DBModel
    static func toPresentationModel(from dbModel: DBModel) -> Model?
}

protocol PresentationConvertible {

    associatedtype Model
    var asPresentationModel: Model? { get }
}

protocol APIConvertible {

    associatedtype Model
    var asAPIModel: Model { get }
}

protocol DatabaseConvertible {

    associatedtype DBModel
    var asDatabaseModel: DBModel { get }
}

