//
//  TestingApp.swift
//  SevAll
//
//  Created by Milos Dimic on 13.05.22.
//

import SwiftUI

struct TestingApp: App {
    
    var body: some Scene {
        
        WindowGroup {
            Text("Tests are running")
        }
    }
}
