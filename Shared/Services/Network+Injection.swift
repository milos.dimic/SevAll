//
//  Network+Injection.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

import Resolver
import SevAllNetworkLayer
import SevAllPersistenceLayer

extension Resolver {
    
    static func registerClients(isTestingUI: Bool) {
        
        if isTestingUI {
            
            self.register {
                APIClientStub()
            }
            .implements(APIClientRepresentable.self)
        } else {
            
            self.register {
                APIClient()
            }
            .implements(APIClientRepresentable.self)
        }
    }
}


extension Resolver {
    
    static func registerNetworkLayer(isTestingUI: Bool) {
        
        if isTestingUI {
            fatalError("please implement")
        } else {
            
            self.register {
                SevAllNetworkLayer.shared
            }
            .implements(SevAllNetworkLayerProtocol.self)
            .scope(.application)
        }
    }
}

extension Resolver {
    
    static func registerPersistenceLayer(isTestingUI: Bool) {
        
        if isTestingUI {
            fatalError("please implement")
        } else {
            
            self.register {
                SevAllPersistenceLayer()
            }
        }
    }
}
