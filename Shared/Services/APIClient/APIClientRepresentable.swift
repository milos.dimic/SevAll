//
//  APIClientRepresentable.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

protocol APIClientRepresentable {
    
    func bar() -> String
}
