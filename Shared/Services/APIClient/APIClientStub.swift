//
//  APIClientStub.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

final class APIClientStub {}

extension APIClientStub: APIClientRepresentable {
    
    func bar() -> String {
        "UI test"
    }
}

