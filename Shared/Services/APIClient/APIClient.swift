//
//  APIClient.swift
//  SevAll
//
//  Created by Milos Dimic on 16.05.22.
//

final class APIClient {}

extension APIClient: APIClientRepresentable {
    
    func bar() -> String {
        "real deal"
    }
}
