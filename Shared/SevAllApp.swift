//
//  SevAllApp.swift
//  Shared
//
//  Created by Milos Dimic on 13.05.22.
//

import SwiftUI

struct SevAllApp: App {
    
    var body: some Scene {
        
        WindowGroup {
            HomeView(viewModel: ViewModelFactory.makeHomeViewModel())
        }
    }
}
