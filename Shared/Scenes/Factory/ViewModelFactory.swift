//
//  ViewModelFactory.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

import Foundation

enum ViewModelFactory {
    
    static func makeHomeViewModel() -> HomeViewModel {
        HomeViewModel()
    }
}
