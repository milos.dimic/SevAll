//
//  HomeView.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//

import SwiftUI

struct HomeView<ViewModel>: View where ViewModel: HomeViewRepresentable {
    
    @StateObject
    var viewModel: ViewModel
    
    var body: some View {
        
        Text("Hello, world!")
            .padding()
        Button(self.viewModel.foo()) {
            self.handledDidTapButtonExecute()
        }
        .padding()
    }
    
    private func handledDidTapButtonExecute() {
        self.viewModel.foo()
    }
}

struct HomeView_Previews: PreviewProvider {
    
    static var previews: some View {
        HomeView(viewModel: ViewModelFactory.makeHomeViewModel())
    }
}
