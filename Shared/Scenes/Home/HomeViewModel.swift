//
//  HomeViewModel.swift
//  SevAll
//
//  Created by Milos Dimic on 15.05.22.
//
import Foundation
import Resolver
import SevAllNetworkLayer
import SevAllPersistenceLayer

protocol HomeViewRepresentable: ObservableObject {
    
    func foo() -> String
}

final class HomeViewModel: HomeViewRepresentable {
    
    @Injected
    private var apiClient: APIClientRepresentable
    
    @Injected
    private var networkLayer: SevAllNetworkLayerProtocol
    
    @Injected
    private var persistenceLayer: SevAllPersistenceLayer
    
    private let helper = Helper()
    
    func foo() -> String {
        
        // Network
        print(#file, #line, SevAllNetworkLayer.shared.uuid)
        print(#file, #line, self.networkLayer.uuid)
        print(#file, #line, self.helper.networkLayer.uuid)
        
        // Persistence
        print(#file, #line, SevAllPersistenceLayer().uuid)
        print(#file, #line, self.persistenceLayer.uuid)
        
        return self.apiClient.bar()
    }
}

final class Helper {
    
    @Injected
    var networkLayer: SevAllNetworkLayerProtocol
}
